## La forge des communs numériques

### La feuille de route des communs numériques

#### Faire vivre et mutualiser les communs numériques sur les territoires

Séminaire DRANE-DRASI, 14-15 mai, Lille

---

<img class="r-stretch" src="media/babyfoot.jpg">

----

<img class="r-stretch" src="media/openai-ign.png">

---

<img class="r-stretch" src="media/laforgeedu-titre-url.png">

---

# Ressource

<img class="r-stretch" src="media/nuit-du-code-groupe.jpg">

----

## Logiciels Libres

----

<img class="r-stretch" src="media/primtux-v8.png">

[PrimTux](https://primtux.fr/) [[source](https://forge.apps.education.fr/primtux)]

----

<img class="r-stretch" src="media/nuitducode.gif">

[La Nuit du Code](https://www.nuitducode.net/) [[source](https://forge.apps.education.fr/nuit-du-code)]

----

<img class="r-stretch" src="media/ecombox.png">

[e-comBox](https://www.reseaucerta.org/pgi/e-combox) [[source](https://forge.apps.education.fr/e-combox)]

----

<img class="r-stretch" src="media/qcmcam.png">

[QCMCam](https://qcmcam.net/) [[source](https://forge.apps.education.fr/sebastien.cogez/qcmcam2)]

----

<img class="r-stretch" src="media/educajou.png">

[Éducajou](https://educajou.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/educajou)]

----

<img class="r-stretch" src="media/mymarkmap2.gif">

[myMarkmap](https://eyssette.forge.apps.education.fr/myMarkmap/) [[source](https://forge.apps.education.fr/eyssette/myMarkmap)]

----

<img class="r-stretch" src="media/flipbook.gif">

[Flipbook](https://eyssette.forge.apps.education.fr/flipbook/) [[source](https://forge.apps.education.fr/eyssette/flipbook)]

----

<img class="r-stretch" src="media/mon-oral.png">

[mon-oral.net](https://www.mon-oral.net/) [[source](https://forge.apps.education.fr/mon-oral)]

----

<img class="r-stretch" src="media/creacarte.png">

[CréaCarte](https://lmdbt.forge.apps.education.fr/creacarte/) [[source](https://forge.apps.education.fr/lmdbt/creacarte)]

----

<img class="r-stretch" src="media/chatmd.png">

[ChatMD](https://eyssette.forge.apps.education.fr/chatMD/) [[source](https://forge.apps.education.fr/eyssette/chatMD)]

----

<img class="r-stretch" src="images/f-markpage.png">

[Markpage](https://eyssette.forge.apps.education.fr/markpage/) [[source](https://forge.apps.education.fr/eyssette/markpage)]

----

<img class="r-stretch" src="media/ubisit.png">

[Ubisit](https://ubisit.forge.apps.education.fr/ubisit-plan-de-classe-aleatoire) [[source](https://forge.apps.education.fr/ubisit/ubisit-plan-de-classe-aleatoire)]

----

<img class="r-stretch" src="media/ecole-inclusive.png">

École Inclusive [[source](https://forge.apps.education.fr/dsoulie/Ecole_inclusive)]

----

## Ressources Éducatives Libres

----

<img class="r-stretch" src="media/fjunier.jpg">

[Le site pro d'un enseignant](https://fjunier.forge.apps.education.fr/tnsi/) [[source](https://forge.apps.education.fr/fjunier/tnsi)]



----

<img class="r-stretch" src="images/f-site-texiotheque.png">

[La TeXiothèque](https://lmdbt.forge.apps.education.fr/latexiotheque/)

----

<img class="r-stretch" src="images/f-site-svt-heredite.png">

[Exercices de génétique humaine](https://bio.forge.apps.education.fr/heredite/)

----

<img class="r-stretch" src="images/f-site-svt-heredite-quiz.gif">

[Exercices de génétique humaine](https://bio.forge.apps.education.fr/heredite/aniridie.html) [[source](https://forge.apps.education.fr/bio/heredite)]

----

<img class="r-stretch" src="images/f-site-snt.png">

[Enseigner la SNT](https://ressources.forge.apps.education.fr/snt/)

----

<img class="r-stretch" src="images/f-site-codex.gif">

[CodEx : le code par les exercices](https://codex.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/codex/codex.forge.apps.education.fr)]

----

<img class="r-stretch" src="media/labomaths.png">

[Laboratoire de mathématiques Grothendieck](https://college-felicien-joly.forge.apps.education.fr/laboratoire-grothendieck/) [[source](https://forge.apps.education.fr/college-felicien-joly/laboratoire-grothendieck)]

----

<img class="r-stretch" src="media/formation-elea-grenoble.png">

[Journée Académique Inspirante Moodle Éléa](https://drane-grenoble.forge.apps.education.fr/jaime/) [[source](https://forge.apps.education.fr/drane-grenoble/jaime)]

----

<img class="r-stretch" src="media/doctrine-technique.png">

[La Doctrine Technique](https://doctrine-technique-numerique.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/doctrine-technique-numerique)]

----

<img class="r-stretch" src="images/f-site-ada.gif">

[Lire en ligne Ada & Zangemann](https://ada-lelivre.fr/) [[source](https://forge.apps.education.fr/nicolas-taffin-ext/ada-lelivre)]

----

<img class="r-stretch" src="media/visite-lycee.png">

[Visite virtuelle de lycée](https://lycee-mounier-grenoble.forge.apps.education.fr/visite-virtuelle/) [[source](https://forge.apps.education.fr/lycee-mounier-grenoble/visite-virtuelle)]

---

# Communauté

<img class="r-stretch" src="media/i-love-la-forge.jpg">

----

## « Un pour tous, tous pour un ! »

----

<img class="r-stretch" src="media/forge-explore.png">

[Une instance GitLab](https://forge.apps.education.fr/explore)

----

## C'est en forgeant...

----

<img class="r-stretch" src="media/mathalea-code.gif">

[Logiciels libres](https://coopmaths.fr/alea/?uuid=ace0a&id=6C10-4&alea=toUT&z=1.5) [[source](https://forge.apps.education.fr/coopmaths/mathalea/-/blob/main/src/exercices/6e/6C10-4.js)]

----

<img class="r-stretch" src="media/site-philo-source-forge.gif">

[Ressources éducatives libres](https://eyssette.forge.apps.education.fr/cours/) [[source](https://forge.apps.education.fr/eyssette/cours/-/blob/main/philo23/src/README.md)]

----

<img class="r-stretch" src="media/mathalea-accueil.png">

L'exemple MathALEA 1

----

<img class="r-stretch" src="media/mathalea-tickets.png">

L'exemple MathALEA 2

----

<img class="r-stretch" src="media/mathalea-request.png">

L'exemple MathALEA 3

----

<img class="r-stretch" src="media/mathalea-membres.png">

L'exemple MathALEA 4

----

## Synergies

----

<img class="r-stretch" src="media/primtux-educajou.png">

[Éducajou dans PrimTux](https://educajou.forge.apps.education.fr/autobd) [[source](https://forge.apps.education.fr/educajou/autobd)]

----

<img class="r-stretch" src="media/elea-documentation.png">

[Documentation d'Éléa](https://dne-elearning.gitlab.io/moodle-elea/documentation/) [[source](https://forge.apps.education.fr/documentation-elea/documentation-elea.forge.apps.education.fr)]

----

<img class="r-stretch" src="media/mathalea-elea.jpg">

[MathALÉA dans Éléa](https://coopmaths.fr/alea/) [[source](https://forge.apps.education.fr/coopmaths/mathalea)]

----

<img class="r-stretch" src="media/mathalea-capytale.png">

[MathALÉA dans Capytale](https://coopmaths.fr/alea/) [[source](https://forge.apps.education.fr/coopmaths/mathalea)]

----

## Animation

----

<img class="r-stretch" src="media/jdle23-ladigitale.png">

[Journée du Libre Éducatif](https://journee-du-libre-educatif.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/journee-du-libre-educatif/journee-du-libre-educatif.forge.apps.education.fr)]

----

<img class="r-stretch" src="media/tchap-laforgeedu.png">

[Tchap](https://www.tchap.gouv.fr/)

----

## Acculturation

----

<img class="r-stretch" src="media/docs-forge.png">

[Documentation de la forge](https://docs.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/docs/docs.forge.apps.education.fr)]

----

<img class="r-stretch" src="media/tube-forge.png">

[Bien commencer avec la Forge](https://tube-numerique-educatif.apps.education.fr/w/vAMyPdtMNRPe8c4TuqhX1d)

----

<img class="r-stretch" src="media/formation-markdown.png">

Formation

---

# Gouvernance

<img class="r-stretch" src="media/migration-forge-benoit-piedallu.jpg">

----

<img class="r-stretch" src="media/portail-apps-education.png">

[apps.education.fr](https://portail.apps.education.fr/)

----

<img class="r-stretch" src="media/stats-laforgeedu-12mai24.png">

Statistiques du 12 mai 2024

----

<img class="r-stretch" src="media/primtux-aic.jpg">

[Soutenir individuellement et collectivement les enseignants](https://communs.beta.gouv.fr/)

----

<img class="r-stretch" src="media/epoc.png">

[ePoc](https://epoc.inria.fr/) [[source](https://forge.apps.education.fr/epoc/e-poc)]

----

<img class="r-stretch" src="media/mooc-inria-forge.png">

[MOOC Inria](https://learninglab.gitlabpages.inria.fr/mooc-impacts-num/mooc-impacts-num-ressources/)

----

<img class="r-stretch" src="media/gtnum-forges.gif">

[GTnum Forges](https://lium.univ-lemans.fr/gtnum-forges/)

----

<img class="r-stretch" src="media/la-suite-numerique.gif">

[Appel à Communs](https://lasuite.numerique.gouv.fr/communs)

----

<img class="r-stretch" src="media/recrutement-forge.png">

[Recrutement](https://choisirleservicepublic.gouv.fr/offre-emploi/cheffe-de-projet-communs-numeriques-et-mixite-dne-tn1-hf-reference-2024-1532199/)

----

<img class="r-stretch" src="media/gouvernance-forge.png">

----

<img class="r-stretch" src="media/regions-academiques.png">

_« Que la forge soit avec nous ! »_

----

<img class="r-stretch" src="media/dane-grenoble.png">

[Dane Grenoble, en force !](https://forge.apps.education.fr/drane-grenoble/drane-grenoble.forge.apps.education.fr/-/project_members)

---

## La feuille de route des communs numériques

<img class="r-stretch" src="media/strategie.png">

----

<img class="r-stretch" src="media/axes-fdr.png">

3 axes

----

<img class="r-stretch" src="media/fdr-ressource1.png">

Ressource (6 actions envisagées)

----

<img class="r-stretch" src="media/fdr-communaute1.png">

Communauté (6 actions envisagées)

----

<img class="r-stretch" src="media/fdr-gouvernance1.png">

Gouvernance (6 actions envisagées)

---

<img class="r-stretch" src="media/commission-ecrans.png">

[« Interdire les écrans » ou « éduquer au numérique » : l’insoutenable alternative (Anne Cordier)](https://theconversation.com/interdire-les-ecrans-ou-eduquer-au-numerique-linsoutenable-alternative-229397)

---

<img class="r-stretch" src="media/alb.png">

---

Merci de votre attention

<img class="r-stretch" src="media/laforgeedu-vignettes.png">

[forge-communs-numeriques@ldif.education.gouv.fr](mailto:forge-communs-numeriques@ldif.education.gouv.fr)

[alexis.kauffmann@education.gouv.fr](mailto:alexis.kauffmann@education.gouv.fr)



